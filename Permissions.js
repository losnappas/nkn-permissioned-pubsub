import isEqual from 'lodash.isequal'
import unionWith from 'lodash.unionwith'
import differenceWith from 'lodash.differencewith'
import { getPubkeyFromAddr } from './util'

function createRule (addr) {
	if (Array.isArray(addr)) {
		if (addr[0] && !addr[0].addr && !addr[0].pubkey) {
			// Regular ['addr1', 'addr2'] array.
			return addr.map((a) => ({ addr: a }))
		}
		// Correct [{'addr': addr}, {'addr': addr1}] format.
		return addr
	} else if (typeof addr === 'string') {
		return [{
			addr
		}]
	} else {
		throw new Error('Creating rule out of what??' + addr)
	}
}

function testPermission (addr, rule) {
	if (rule.pubkey) {
		return getPubkeyFromAddr(addr) === rule.pubkey
	} else if (rule.addr) {
		return addr === rule.addr
	}
	return false
}

class Permissions {
	constructor ({ accept = [], reject = [] } = {}) {
		this.accept = accept
		this.reject = reject
	}

	// Add permission.
	union ({ accept = [], reject = [] } = {}) {
		const a = unionWith(
			this.accept,
			createRule(accept),
			isEqual
		)
		const r = unionWith(
			this.reject,
			createRule(reject),
			isEqual
		)
		return new Permissions({ accept: a, reject: r })
	}

	// Remove permission.
	pull ({ accept = [], reject = [] } = {}) {
		const a = differenceWith(
			this.accept,
			createRule(accept),
			isEqual
		)
		const r = differenceWith(
			this.reject,
			createRule(reject),
			isEqual
		)
		return new Permissions({ accept: a, reject: r })
	}

	isEqual (against) {
		return isEqual(against, this.values())
	}

	/**
	 * Filters subscribers against this.
	 *
	 * @param [String] subs Subscribers.
	 * @return [String] Allowed subs.
	 */
	against (subs) {
		if (subs.length === 0) {
			return []
		}

		const allows = this.accept
		const rejects = this.reject

		if (allows.length === 0) {
			return []
		}

		let subsAllowed = subs
		if (allows.every((perm) => perm.addr !== '*')) {
			subsAllowed = allows.reduce((acc, rule) => {
				if (rule.pubkey) {
					return acc.concat(subs.filter(sub => getPubkeyFromAddr(sub) === rule.pubkey))
				} else if (rule.addr) {
					return subs.includes(rule.addr) && !acc.includes(rule.addr)
						? acc.concat(rule.addr) : acc
				}
				return acc
			}, [])
		}

		if (rejects.length > 0) {
			// Removes all rejects from array.
			subsAllowed = differenceWith(subsAllowed, rejects, testPermission)
		}
		return subsAllowed
	}

	test (addr) {
		if (!addr) {
			throw new Error(`Addr was falsy: ${addr}`)
		}
		return this.against([addr]).includes(addr)
	}

	values () {
		const v = {
			accept: [...this.accept],
			reject: [...this.reject]
		}
		return v
	}

	get length () {
		return this.accept.length + this.reject.length
	}
}

export default Permissions
