const createErr = topic => new Error(`Topic "${topic}" is not a valid permissioned topic. Correct format is 'name.pubkey'.`)

const parsePermissionedTopic = topic => {
	if (!topic) {
		throw createErr(topic)
	}
	const lastDotPosition = topic.lastIndexOf('.')
	let pubkey = topic
	let thisTopic = ''
	if (lastDotPosition !== -1) {
		thisTopic = topic.substring(0, lastDotPosition)
		pubkey = topic.slice(lastDotPosition + 1)
	}
	if (!isPubkey(pubkey)) {
		throw createErr(topic)
	}
	return [thisTopic, pubkey]
}

export function getAdmin (topic) {
	try {
		const [, admin] = parsePermissionedTopic(topic)
		return admin
	} catch (e) {
		return false
	}
}

function isPubkey (addr) {
	return addr.length === 64
}
export function getPubkeyFromAddr (addr) {
	const a = addr.slice(addr.lastIndexOf('.') + 1)
	if (!isPubkey(a)) {
		throw new Error('Not an addr:' + addr)
	}
	return a
}

export const isTopicAdmin = (topic, addr) => getAdmin(topic) === getPubkeyFromAddr(addr)
export const isPermissionedTopic = topic => !!getAdmin(topic)
