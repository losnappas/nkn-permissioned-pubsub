# nkn-permissioned-pubsub

Permissioned pubsub handler. Check out NKP-0016.

## Installation

`npm install https://gitlab.com/losnappas/nkn-permissioned-pubsub`

## Usage

```javascript
import { MultiClient, Wallet } from 'nkn-sdk';
import permissionify from 'nkn-permissioned-pubsub';

const wallet = Wallet.newWallet('pw');
const client = new Nkn;
client.getNonce = wallet.getNonce;
client.subscribe = wallet.subscribe;
const permissions = permissionify(client);
```

Or with mixin

```javascript
import { MultiClient } from 'nkn-sdk';
import permissionifyMixin from 'nkn-permissioned-pubsub/mixin';

function genChatID(topic) {
	return 'dchat' + shasum(topic);
}

class MyNKNWrapper extends permissionifyMixin(MultiClient) {
	constructor(wallet) {
		super({
			numSubClients: 3,
			originalClient: false,
			msgCacheExpiration: 300 * 1000,
		})
		this.wallet = wallet;
	}
	
	/** These 3 methods have to exist! */
	getNonce() {
		return this.wallet.getNonce();
	}
	getSubscribers(topic, opts = {}) {
		// Multiclient stuff.
		return this.defaultClient.getSubscribers(genChatID(topic), opts));
	}
	subscribe(topic, opts = {}) {
		if (opts.settingPermissions) {
			// Maybe ignore subscription status?
		}
		return this.wallet.subscribe(
			genChatID(topic),
			10, // blocks
			'my-identifier',
			// Must stringify.
			JSON.stringify(opts.metadata),
			opts
		);
	}
}
const x = new MyNKNWrapper();
console.log(x.Permissions.getPermissions('topic'));
```

We are overriding `getSubscribers` and `subscribe` because we want to run `genChatID` in D-Chat.

## Methods

If you call `Permissions.accept(...)`, `.reject`, or `.remove` too rapidly in succession, it will overwrite previous ones.

See [index.js](./index.js).
