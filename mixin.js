import permissionify from './index'

const permissionedMixin = NknClient => class extends NknClient {
	constructor(...args) {
		super(...args)
		this.Permissions = permissionify(this)
	}
}

export default permissionedMixin
