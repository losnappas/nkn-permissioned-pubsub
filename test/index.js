import test from 'tape'
import { getPubkeyFromAddr } from '../util'
import Permissions from '../Permissions'
import mixin from '../mixin'

const subs = [
	'123.aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
	'bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb',
	'test.cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc',
	'admin.dddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd',
	'pk already exists.aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
	'xd already exists.aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
	'1xd.aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
	'2xd.aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
	'3xd.aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
	'4xd.aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
	'5xd.aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
	'6xd.aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'
]

test('Basic permissions test', t => {
	t.plan(2)

	const perms = {
		accept: [{
			addr: subs[0]
		}, {
			pubkey: getPubkeyFromAddr(subs[2])
		}, {
			addr: subs[3]
		}],
		reject: []
	}
	const permissions = new Permissions(perms)

	t.equal(JSON.stringify(perms), JSON.stringify(permissions))

	const allowedSubs = permissions.against(subs)
	t.deepEqual([
		subs[0],
		subs[2],
		subs[3]
	], allowedSubs)
})

test('Permissions with pk test', t => {
	t.plan(2)

	let perms = {
		accept: [{
			pubkey: getPubkeyFromAddr(subs[4])
		}],
		reject: []
	}
	let permissions = new Permissions(perms)
	let allowedSubs = permissions.against(subs)
	t.deepEqual([
		subs[0],
		subs[4],
		subs[5],
		subs[6],
		subs[7],
		subs[8],
		subs[9],
		subs[10],
		subs[11]
	].sort(), allowedSubs.sort())

	perms = {
		accept: [{
			pubkey: getPubkeyFromAddr(subs[4])
		}, {
			addr: subs[4]
		}, {
			addr: subs[1]
		}],
		reject: []
	}
	permissions = new Permissions(perms)
	allowedSubs = permissions.against(subs)
	t.deepEqual([
		subs[0],
		subs[4],
		subs[5],
		subs[6],
		subs[7],
		subs[8],
		subs[9],
		subs[10],
		subs[11],
		subs[1]
	].sort(), allowedSubs.sort())
})

test('Permissions pull test', t => {
	t.plan(1)
	const perms = {
		accept: [{
			addr: subs[4]
		}],
		reject: []
	}
	const permissions = new Permissions(perms).pull({ accept: [subs[4]] })

	t.equal(permissions.accept.length, 0)
})

test('Permissions union test', t => {
	t.plan(1)
	const perms = {
		accept: [{
			addr: subs[4]
		}],
		reject: []
	}
	const permissions = new Permissions(perms).union({ accept: [subs[1]] })

	t.deepEqual([
		{
			addr: subs[4]
		},
		{
			addr: subs[1]
		}
	], permissions.accept)
})

test('Reject permission test', t => {
	t.plan(1)
	const perms = {
		accept: [{
			addr: subs[4]
		}, {
			addr: subs[3]
		}],
		reject: [{
			addr: subs[4]
		}, {
			addr: subs[0]
		}]
	}
	const permissions = new Permissions(perms)

	const allow = permissions.against(subs)
	t.deepEqual([subs[3]], allow, 'allows correct addrs')
})

const Client = class extends mixin(Object) {
	constructor () {
		super()
		this._rules = {}
		this.rules = {
			'__0__.__permission__.aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa': JSON.stringify({
				accept: [
					{ addr: subs[0] },
					{ addr: subs[1] },
					{ addr: subs[2] },
					{ addr: subs[3] },
					{ addr: subs[4] },
					{ addr: subs[5] },
					{ addr: subs[6] },
					{ addr: subs[7] },
					{ addr: subs[8] },
					{ addr: subs[9] }
				]
			}),
			[subs[0]]: ''
		}
	}

	getSubscribers () {
		return { subscribers: this.rules, subscribersInTxPool: [] }
	}

	async getNonce () {
		return 1
	}

	async subscribe (_, {
		metadata = '',
		identifier = ''
	} = {}) {
		// Normally this would be added by nkn itself, of course.
		identifier = identifier.startsWith('__') ? identifier + '.aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa' : identifier
		this.rules[identifier] = JSON.stringify(metadata)
		this._rules[identifier] = metadata
	}
}

test('Having over 10 permissions test', async t => {
	// t.plan(5)
	const perms = {
		accept: subs.slice(0, 12).map(sub => ({ addr: sub })),
		reject: []
	}
	const topic = 'topic.aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'

	const nkn = new Client()
	await nkn.Permissions.accept(topic, subs[0])

	const permissions = new Permissions(perms)
	const removed = permissions.pull({
		accept: [
			{ addr: '6xd.aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa' }
		]
	})
	t.ok(removed.accept.length === 11, 'Removed a permissions correctly.')

	let gets = await nkn.Permissions.getSubscribers(topic)
	t.ok(gets.length === 1 && gets[0] === subs[0], 'Only subs[0] is accepted')

	await nkn.Permissions.accept(topic, 'gg.xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx')

	await nkn.Permissions.reject(topic, subs[0])
	gets = await nkn.Permissions.getSubscribers(topic)
	t.deepEqual(gets, [], 'subs[0] is subscribed & accepted, but rejected')

	await nkn.Permissions.remove(topic, subs[0])
	gets = await nkn.Permissions.getSubscribers(topic)
	t.deepEqual(gets, [], 'subs[0] is subscribed but not accepted or rejected')

	await nkn.Permissions.accept(topic, subs[0])
	gets = await nkn.Permissions.getSubscribers(topic)
	t.ok(gets.length === 1 && gets[0] === subs[0], 'subs[0] is accepted')

	/** Then let's test adding many subs at once. */
	const nextSubs = subs.map(sub => 'x' + sub)
	await nkn.Permissions.accept(topic, nextSubs)
	await Promise.all(nextSubs.map(s => nkn.subscribe(topic, { identifier: s })))

	gets = await nkn.Permissions.getSubscribers(topic)
	t.deepEqual(gets, [
		subs[0],
		...nextSubs
	], 'over 10 subs work correctly.')

	// console.log()
	// console.log(await nkn.getSubscribers(topic))
	// console.log()
	// console.log(await nkn.Permissions.getPermissions(topic))

	t.end()
})
