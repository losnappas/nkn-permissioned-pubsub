/**
 * Tests having two subscriptions from one wallet in mempool.
 * Quite unrelated to the lib.
 */
import nknWallet from 'nkn-wallet'
import Nkn from 'nkn-multiclient'
import test from 'tape'
import permissionify from '../'
import sleep from 'sleep-promise'

const client = new Nkn({
	seed: '2bc5501d131696429264eb7286c44a29dd44dd66834d9471bd8b0eb875a1edb0',
	seedRpcServerAddr: 'http://mainnet-seed-0001.nkn.org:30003'
})

nknWallet.configure({
	rpcAddr: 'http://mainnet-seed-0001.nkn.org:30003'
})
const wallet = nknWallet.restoreWalletBySeed(
	'2bc5501d131696429264eb7286c44a29dd44dd66834d9471bd8b0eb875a1edb0',
	'x'
)

// Add methods for permissions
client.subscribe = (topic, opts = {}) => wallet.subscribe(topic, 10, opts.identifier, JSON.stringify(opts.metadata))
client.getSubscribers = (topic, opts = {}) =>
	client.defaultClient.getSubscribers(topic, { txPool: true, meta: false, offset: 0, limit: 1000, ...opts })
client.getNonce = () => wallet.getNonce()

const topic = 'x' + Date.now() + '.8488c5ee3010ec45989ffcbf5c74283e23d0f18c4f8e9ea2f6adb1a942dc8d74'
// const topic = 'x' + Date.now() + '.8488c5ee3010ec45989ffcbf5c74283e23d0f18c4f8e9ea2f6adb1a942dc8d74'
const id1 = 'a' + Date.now()
const id2 = 'b' + Date.now()
const id3 = 'c' + Date.now()
// const addr1 = id1 + '.8488c5ee3010ec45989ffcbf5c74283e23d0f18c4f8e9ea2f6adb1a942dc8d74'
const addr2 = id2 + '.8488c5ee3010ec45989ffcbf5c74283e23d0f18c4f8e9ea2f6adb1a942dc8d74'
const addr3 = id3 + '.8488c5ee3010ec45989ffcbf5c74283e23d0f18c4f8e9ea2f6adb1a942dc8d74'
let Permissions

// const getsubs = () => client.getSubscribers(topic, { txPool: true, meta: true })
const subscribe = (identifier) => wallet.subscribe(topic, 40, identifier)

test('creating permissions', t => {
	t.plan(1)
	Permissions = permissionify(client)

	client.on('connect', () => {
		t.ok(Permissions, 'permissions exist')
	})
})

test('creating a permission', async t => {
	t.plan(1)
	await Permissions.accept('xx' + topic, addr2 + 'testx')
	await sleep(1000)
	const perms = await Permissions.getPermissions('xx' + topic)
	t.ok(perms.accept.length > 0, 'permission exists')
})

test('adding permissions', async t => {
	t.plan(5)
	// await Permissions.accept(topic, addr1)
	await subscribe(id1)

	let subs

	subs = await Permissions.getSubscribers(topic)
	t.ok(subs.length === 0, 'id1 is subbed but not allowed')

	await subscribe(id2)
	t.ok((await Permissions.getSubscribers(topic)).length === 0, 'id{1,2} is subbed but not permitted')

	await Permissions.accept(topic, addr2)
		.catch(err => console.log('accept topic,addr2 error:', err, topic, addr2))

	console.log('accepting addr2. sleeping')
	await sleep(5000)

	const p = await Permissions.getPermissions(topic)
	t.ok(p.accept.length > 0, 'Permission was added')

	subs = await Permissions.getSubscribers(topic)
	t.deepEqual(subs, [
		addr2
	], 'id2 is subbed and allowed, while id1 is subbed but no allowed')
	console.log('sleeping to allow a block to go by so we can test in-block perms.')
	await sleep(40000)
	try {
		console.log('allowing addr3. subscribing addr3')
		// Throws "duplicate sub exists in block"
		await Permissions.accept(topic, addr3)
		await subscribe(id3)
		console.log('waiting for subscription to resolve, though not sure why it takes so long')
		await sleep(80000)
		subs = await Permissions.getSubscribers(topic)
		t.deepEqual(subs, [
			addr2,
			addr3
		], 'id{2,3} is allowed')
	} catch (e) {
		console.log('Threw during addr3 things.')
		t.error(e)
		t.end()
	}
})

test('teardown', t => {
	t.ok(typeof client.close === 'function', 'close function exists.')
	client.close()
	t.end()
})
