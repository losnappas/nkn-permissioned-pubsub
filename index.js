import Permissions from './Permissions'
import sleep from 'sleep-promise'
import { getPubkeyFromAddr, getAdmin } from './util'

// starts with __${n}__.__permission__.
const PERMISSION_REGEX = /^__(\d+)__\.__permission__\./
// Max 10 permissions per subscription.
const CHUNK_SIZE = 10

const pubkeysMatch = (addr1, addr2) => getPubkeyFromAddr(addr1) === getPubkeyFromAddr(addr2)
const parseSubsAndMetadata = r => Object.entries(r.subscribers).concat(Object.entries(r.subscribersInTxPool))
const getIndexFromAddr = addr => {
	const match = PERMISSION_REGEX.exec(addr)
	if (match) {
		return match[1]
	}
}
/**
 * Handles permissions for permissioned pubsub.
 *
 * The NknClient object you pass here should have methods
 *  getNonce,
 *  getSubscribers,
 *  subscribe,
 * and they should have the same signatures as nkn-client and nkn-wallet.
 *
 * See https://gitlab.com/losnappas/d-chat/blob/master/src/workers/nkn/nkn.js or test folder for example.
 *
 * @param object NknClient Nkn client with getNonce, getSubscribers, and subscribe methods added.
 * @return object Permissions handler.
 */
const permissionify = function (NknClient) {
	const nonces = {}

	const Permissioner = {
		/**
		 * Gets permissions from the blockchain.
		 *
		 * @param String topic Topic.
		 * @param [[String, String]] subs Optional. Array of [sub, metadata] entries.
		 * @return Object Permissions object.
		 */
		async getPermissions (topic, subs) {
			const admin = getAdmin(topic)
			if (!admin) {
				// Returns allow-all if topic is not permissioned.
				return new Permissions({ accept: [{ addr: '*' }] })
			}
			const permissions = await this._getPermissionsChunks(topic, subs)
			// Combine into one.
			return permissions.reduce(
				(acc, perms) => acc.union(perms),
				new Permissions()
			)
		},

		async _getPermissionsChunks (topic, subs) {
			const admin = getAdmin(topic)
			if (!subs) {
				subs = await getSubs(topic)
			}

			// Filter out anything that isn't '__n__.__permission__.{admin_pk}' identifier.
			const inBlock = subs.filter(r =>
				PERMISSION_REGEX.test(r[0]) && pubkeysMatch(r[0], admin)
			).map(([addr, meta]) => ([addr, JSON.parse(meta)]))

			// Get permissions objects from metadata.
			const permissionsInBlock = inBlock.reduce((acc, [addr, perms]) => {
				const idx = getIndexFromAddr(addr)
				acc[idx] = new Permissions(perms)
				return acc
			}, [])

			return permissionsInBlock
		},

		/**
		 * @param String topic Topic.
		 * @return [String] Array of allowed subs.
		 */
		async getSubscribers (topic) {
			const subsAndMetadata = await getSubs(topic)

			const permissions = await this.getPermissions(topic, subsAndMetadata)
			const subs = subsAndMetadata.map(([sub]) => sub)

			const allowedSubs = permissions.against(subs)
			return allowedSubs
		},

		/**
		 * @param String topic Topic.
		 * @param String addr Addr.
		 * @return Boolean True if addr is allowed in topic, false otherwise.
		 */
		async check (topic, addr) {
			const admin = getAdmin(topic)
			if (admin) {
				const permissions = await this.getPermissions(topic)
				return permissions.test(addr)
			}
			// This isn't a permissioned topic, everything goes.
			return true
		},

		/**
		 * Accepts an addr to chat.
		 *
		 * Does not work out if you call this many times in a row. Use an array instead.
		 *
		 * @param String topic Topic.
		 * @param (String|[String]) addr Address or array of addresses.
		 * @return Promise<[String|null]> Subscription promises.
		 */
		async accept (topic, addr) {
			return this.adds(topic, {
				accept: mapAddrs(addr)
			})
		},

		/**
		 * Rejects an addr to chat.
		 *
		 * Does not work out if you call this many times in a row. Use an array instead.
		 *
		 * @param String topic Topic.
		 * @param (String|[String]) addr Address or array of addresses.
		 * @return Promise<[String|null]> Subscription promises.
		 */
		async reject (topic, addr) {
			return this.adds(topic, {
				reject: mapAddrs(addr)
			})
		},

		/**
		 * Removes an addr from chat permissions, essentially rejecting them.
		 *
		 * Does not work out if you call this many times in a row. Use an array instead.
		 *
		 * @param String topic Topic.
		 * @param (String|[String]) addr Address or array of addresses.
		 * @return Promise<[String|null]> Subscription promises.
		 */
		async remove (topic, addr) {
			return this.removes(topic, {
				reject: mapAddrs(addr),
				accept: mapAddrs(addr)
			})
		},

		/**
		 * Adds a bunch of permissions.
		 *
		 * @param String topic Topic.
		 * @param Object perms Accepts/rejects to be added.
		 * @return Promise Subscription promises.
		 */
		async adds (topic, perms) {
			let permsToAdd = new Permissions(perms)
			// We are only adding 1 permission at a time, so going easy here.
			let permissions = await this._getPermissionsChunks(topic)
			// First, let's remove existing permissions from our list of new ones.
			permissions.forEach(existingPermissions => {
				permsToAdd = permsToAdd.pull(existingPermissions)
			})
			if (permsToAdd.length === 0) {
				return
			}

			// Now, if we've still got something left, we want to add it to a chunk.
			let added = false
			permissions = permissions.map(permission => {
				if (!added && (permission.length + permsToAdd.length) < CHUNK_SIZE) {
					added = true
					return permission.union(permsToAdd)
				}
				return null
			})
			if (!added) {
				permissions.push(permsToAdd)
			}

			return this.commits(topic, permissions)
		},

		/**
		 * Removes a bunch of permissions.
		 *
		 * @param String topic Topic.
		 * @param Object perms Accepts/rejects to be removed.
		 * @return Promise Subscription promises.
		 */
		async removes (topic, perms) {
			const permissions = (await this._getPermissionsChunks(topic)).map(p => {
				const thing = p.pull(perms)
				return thing.isEqual(p) ? null : thing
			})
			return this.commits(topic, permissions)
		},

		/**
		 * Commits permissions in a subscription tx.
		 *
		 * @param String topic Topic.
		 * @param Object perms Accepts/rejects to be removed.
		 * @return [Promise] Subscriptions promises.
		 */
		async commits (topic, permissions) {
			return Promise.all(
				permissions.map((permission, index) => {
					if (!permission) {
						return
					}
					return subscribe({
						topic,
						metadata: permission.values(),
						index
					})
				})
			)
		}
	}

	/** Internal stuff. */

	async function subscribe ({ topic, metadata, index }) {
		return NknClient.subscribe(topic, {
			metadata,
			identifier: `__${index}__.__permission__`,
			settingPermissions: true,
			nonce: await getNonce(topic, index)
		}).catch(async () => {
			await sleep(100)
			// Nonce was probably bad. Try again with new nonce.
			return NknClient.subscribe(topic, {
				metadata,
				identifier: `__${index}__.__permission__`,
				settingPermissions: true,
				nonce: await getNonce(topic, index, await NknClient.getNonce())
			})
		})
	}

	async function getSubs (topic) {
		const subs = await NknClient.getSubscribers(topic, {
			meta: true,
			txPool: true
		})
		const subsAndMeta = parseSubsAndMetadata(subs)
		return subsAndMeta
	}

	async function getNonce (topic, index, newNonce) {
		nonces[topic] = nonces[topic] || {}
		nonces[topic][index] = newNonce || nonces[topic][index] || await NknClient.getNonce()
		return nonces[topic][index]
	}

	return Permissioner
}

function mapAddrs (addr) {
	if (Array.isArray(addr)) {
		return addr.map(addr => ({ addr }))
	} else {
		return [{ addr }]
	}
}

export default permissionify
